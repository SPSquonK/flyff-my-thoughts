# Playing with multiple accounts

To me, FlyFF means "multi account".

The game requires you by design to have multiple characters:

-    You need a leech to level up
-    You need a full party to deal more damage and earn more experience (and more often than not, you can not trust other party leaders because there is always something missing, or they randomly kicks you out of the party, ...)
-    You need a character that sits there to tank the boss
-    You need a character to deal damage to the boss while not looking at the window
-    You need a character to heal the "character that sits there" if Remantis Laccotts are disabled for some reasons


To play, you have to rely on other characters, and these characters don't do very complex tasks. Why look for 5 other persons to do some very basic tasks, when you can do them yourself and play whenever you want?

Straight up forbidding multi accounts is dumb and does not solve the core issue: if you want player to not multi account, they should not have the time to manage multiple characters. Consider any MOBA game like League of Legends: even if you want to multi account, you just can't by design of the game because one character makes you too busy.
